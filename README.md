# God from the Machine

A universal virtual-appearance operator, originally intended to reduce smart assistants' complication, then found this could be used in a more useful and cute purpose. The purpose is realized by specifically handle non-AI related actions like movements, emotions, simple communications, etc, and only communicates with the AI in simple commands like "affection_love_precise" or "01092871". Provides a shared library for accessing the framework, model creation rules, the framework and a pre-process program.

Actually I'm not sure if calling it an "operator" is right enough, let it be handler or server or framework, whatever. Anyway it deals with graphics, physics, phsycology, anime, anything but the AI computing.

I want this to be a stand alone graphical frontend for any compatible AI, thus allows users to customize their own AI assistant without having to deal with "cloud", "megacorps" or compromise their privacy. However the usage is never limited, I think animators, vtubers, anime-lovers and manymore people will find this useful.
